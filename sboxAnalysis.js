const fs = require('fs');
const streamData = fs.readFileSync('./sbox.sbx');

const sboxFunctions = extractFunctionsFromSbox(8, streamData);
checkFunctionsBalance(sboxFunctions);
const allLinearFunctionsSet = generateLinearFunctionsSet(8);
hammingDistance(sboxFunctions, allLinearFunctionsSet);
checkCycles(streamData);
SAC(8, sboxFunctions);
xorProfile(8, streamData);

function extractFunctionsFromSbox(numOfInputs, streamData) {
    const binaryFunctionsInString = [];
    let sboxFunctions;

    streamData.forEach((data, index) => {

        if (index % 2 !== 1) {

            let chunk = data.toString();
            let binNum = dec2bin(chunk);

            if (binNum.length < 8) {
                binNum = fillWithZeros(binNum);
            }
            binaryFunctionsInString.push(binNum);
        }
    })
    sboxFunctions = changeStringArrToNumArr(numOfInputs, binaryFunctionsInString);
    return sboxFunctions;
}

function changeStringArrToNumArr(numOfInputs, binaryFunctionsInString) {
    let calculatedFunctions = [];

    for (let i = 0; i < numOfInputs; i++) {

        calculatedFunctions[i] = [];
        binaryFunctionsInString.forEach((v) => {
            calculatedFunctions[i].push(Number(v[i]));
        })
    }
    return calculatedFunctions;
}

function fillWithZeros(binNum) {
    let missingChars = 8 - binNum.length;
    return '0'.repeat(missingChars).concat(binNum);
}

function checkFunctionsBalance(sboxFunctions) {
    let zerosCounter = 0, onesCounter = 0;

    for (let i = 0; i < 8; i++) {

        sboxFunctions[i].forEach(v => {

            switch (v) {
                case 0:
                    zerosCounter++;
                    break;
                case 1:
                    onesCounter++;
                    break;
            }
        })
        console.log(`Function no. ${7 - i} balance: zeros - ${zerosCounter}, ones - ${onesCounter}`)
        zerosCounter = 0;
        onesCounter = 0;
    }
    console.log('\n');
}

function createBinArray(numOfInputs) {
    const binaryFunctionsInString = [];
    let numArr;

    for (let i = 0; i < Math.pow(2, numOfInputs); i++) {

        let binNum = dec2bin(i)
        if (binNum.length < 8) {
            binNum = fillWithZeros(binNum);
        }
        binaryFunctionsInString.push(binNum);
    }
    numArr = changeStringArrToNumArr(numOfInputs, binaryFunctionsInString)
    return numArr;
}

function dec2bin(dec) {
    return (dec >>> 0).toString(2)
}

function initZeroArray(numOfInputs) {
    return Array(Math.pow(2, numOfInputs)).fill(0);
}

function initOneArray(numOfInputs) {
    return Array(Math.pow(2, numOfInputs)).fill(1);
}

function xorFunctions(firstArr, secondArr) {
    let xorArr = [];

    for (let i in firstArr)
        xorArr.push(Number(firstArr[i]) ^ Number(secondArr[i]));

    return xorArr;
}

function generateLinearFunctionsSet(numOfInputs) {
    const allBinFunctions = createBinArray(numOfInputs);
    let singleArgsArr = [], finalSet = [], tempArr = [];
    let zeroArr = initZeroArray(numOfInputs);
    let oneArr = initOneArray(numOfInputs);

    for (let i = 0; i < numOfInputs; i++) {
        singleArgsArr[i] = [];
        allBinFunctions[i].forEach((v) => {
            singleArgsArr[i].push(v)
        })
    }

    for (let i = 0; i < Math.pow(2, numOfInputs); i++) {
        tempArr = zeroArr;

        for (let j = 0; j < numOfInputs; j++) {

            if ((i & Math.pow(2, j))) {
                tempArr = xorFunctions(tempArr, singleArgsArr[j])
            }
        }
        finalSet.push(tempArr);
        finalSet.push(xorFunctions(tempArr, oneArr))

        finalSet.sort((a, b) => a.length - b.length)
    }
    return finalSet;
}

function hammingDistance(sboxFunctions, allLinearFunctionsSet) {
    let distanceArr = [], minDistance, counter;

    sboxFunctions.forEach((sFunction) => {
        minDistance = Number.MAX_VALUE;
        allLinearFunctionsSet.forEach((lFunction) => {

            counter = 0;

            for (let i = 0; i < sFunction.length; i++) {

                if (sFunction[i] ^ lFunction[i] === 1)
                    counter++;
            }
            if (minDistance > counter)
                minDistance = counter;
        })
        distanceArr.push(minDistance);
    })

    for (let i in distanceArr)
        console.log(`Non Linearity for function number ${distanceArr.length - 1 - i}: ${distanceArr[i]}`)

    console.log('\n');
}

function getAllNumsFromSbox(streamData) {
    const numArr = [];

    streamData.forEach((data, index) => {
        if (index % 2 === 0) {
            numArr.push(data);
        }
    })
    return numArr
}

function checkCycles(streamData) {
    const numArr = getAllNumsFromSbox(streamData);
    let randomInput = Math.floor(Math.random() * numArr.length);
    let inputToBeChecked = randomInput;
    let numOfCycles = 0;

    for (let i in numArr) {
        randomInput = numArr[randomInput];

        if (randomInput === inputToBeChecked)
            numOfCycles++;
    }
    console.log(`Number of cycles: ${numOfCycles}\n`);
}

function SAC(numOfInputs, sboxFunctions) {
    let functionsWithChangedBit = [];
    let probabilityForFunctions = new Array(8).fill(0);
    let alpha, counter;

    sboxFunctions.forEach((v, i) => {
        functionsWithChangedBit[i] = [];
        alpha = 1;

        for (let j = 0; j < numOfInputs; j++) {
            functionsWithChangedBit[i][j] = [];

            for (let k = 0; k < v.length - alpha; k++) { // k - index of bit in function
                functionsWithChangedBit[i][j][k] = v[k + alpha];
                functionsWithChangedBit[i][j][k + alpha] = v[k];
            }
            alpha *= 2;
        }

        for (let j = 0; j < numOfInputs; j++) {
            counter = 0;

            for (let k = 0; k < v.length; k++) { // k - index of bit in function
                if (v[k] === functionsWithChangedBit[i][j][k])
                    counter++
            }
            probabilityForFunctions[i] += counter / Math.pow(2, numOfInputs);
        }
        probabilityForFunctions[i] = Math.round((probabilityForFunctions[i] / 8) * 100);
    })

    probabilityForFunctions.forEach((v, i) => {
        console.log(`SAC for function number ${numOfInputs - 1 - i}: ${v}%`)
    })
    console.log('\n');
}

function comibnationsOfTwoNums(array) {
    return array.flatMap(
        (v, i) => array.slice(i + 1).map(w => [v, w])
    );
}

function xorTwoNums(first, second) {
    return first ^ second;
}

function xorProfile(numOfInputs, streamData) {
    let matrix = [];
    const numberOfRounds = Math.pow(2, numOfInputs);
    const numArr = Array.from({ length: numberOfRounds }, (v, k) => k);
    const allNumsCombinations = comibnationsOfTwoNums(numArr);
    const allNumsFromSbox = getAllNumsFromSbox(streamData);
    let y0, y1, xorResultY, xorResultX;
    let maxValueInMatrix = 0;

    for (let i = 0; i < numberOfRounds; i++) {
        matrix[i] = [];
        for (let j = 0; j < numberOfRounds; j++)
            matrix[i][j] = 0;
    }

    allNumsCombinations.forEach((v, i) => {
        y0 = allNumsFromSbox[v[0]];
        y1 = allNumsFromSbox[v[1]];
        xorResultY = xorTwoNums(y0, y1);
        xorResultX = xorTwoNums(v[0], v[1]);
        matrix[xorResultX][xorResultY] += 2;
    })

    for (let i = 0; i < numberOfRounds; i++)
        for (let j = 0; j < numberOfRounds; j++)
            if (matrix[i][j] > maxValueInMatrix)
                maxValueInMatrix = matrix[i][j];

    console.log(`XOR profile: ${maxValueInMatrix}\n`)
}