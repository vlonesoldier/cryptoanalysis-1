# Cryptoanalysis 1

1. Open the file sbox.sbx in binary editor and read the functions written in it.
2. Check the balance of each function. Is this feature important from a cryptographic point
of view? Explain why this is important.
3. Determine the nonlinearity of this functions. To do so, generate the set of all 8-argument
affine functions. What is the size of this set?
4. Examine cyclicity of sbox.
5. Verify that the strict avalanche criterion (SAC) is satisfied for each function. What value
of the probability of change in the output was obtained for the entire block?
6. Write a short report from the class. It should include the obtained results: nonlinearity
of the block, yes/no balancing, SAC, description of the method of generating the set of
affine functions, summary.